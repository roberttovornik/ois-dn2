function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

    //prepreci napade XSS
    sporocilo = sporocilo.replace(/</g, "&lt").replace(/>/g, "&gt").replace(/'/g, "&#39;").replace(/"/g, "&#34;");    
    
    //uredi sporocilo
    sporocilo = smeski(sporocilo);
   
    var kanalBesede = $('#kanal').text().split(' ');
    var zadnja = kanalBesede[kanalBesede.length - 1];
    
    klepetApp.posljiSporocilo(zadnja, sporocilo); 
    
    $('#sporocila').append(divElementHtmlTekst(sporocilo)); 

    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function smeski(besedilo) {

  var smeski = [":)",";)",":(",":*","(y)"];
  var smeski_link = ['smiley', 'wink', 'sad', 'kiss', 'like'];
  
  var smeski_url = 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/'
  
  for(var i =0; i<smeski.length; i++)
  {
    
    //upostevaj escape-e posebnih znakov za RegExp
    var znak = smeski[i].replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
    znak = new RegExp(znak, 'g');
    
    var smeski_slika = '<img src="' + smeski_url + smeski_link[i] + '.png" />';
    besedilo = besedilo.replace(znak, smeski_slika);
    
  }
  return besedilo;
  
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var usrName;
  var chnnlName;

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      usrName = rezultat.vzdevek;
      $('#kanal').text(usrName + ' @ '  + chnnlName);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    chnnlName = rezultat.kanal;
    $('#kanal').text(usrName + ' @ '  + chnnlName);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html('<i>'+sporocilo.besedilo+'</i>');
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});